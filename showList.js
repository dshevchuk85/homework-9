const firstTestArr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const secondTestArr = ["1", "2", "3", "sea", "user", 23];

function changeArrStrToArrHtml (input) {
    return input.map(elem => {
        if(Array.isArray(elem)) return `<ul>${changeArrStrToArrHtml(elem)}</ul>`;
        return `<li>${elem}</li>`;
    }).join('');
}

function showListOnPage (input, parent = document.body) {
    parent.insertAdjacentHTML('beforeend', `<ul>${changeArrStrToArrHtml(input)}</ul>`);
}


showListOnPage(firstTestArr);
showListOnPage(secondTestArr);






